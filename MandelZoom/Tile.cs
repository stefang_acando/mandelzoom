﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks.Schedulers;
using System.Numerics;

namespace MandelZoom
{
    public class Tile : IDisposable
    {
        public const int TilePixels = 128;
        private Bitmap _bitmap;
        public Bitmap Bitmap { 
            get {
                return _bitmap;
            } 
            set {
                Bitmap oldBitmap;
                lock (this)
                {
                    oldBitmap = _bitmap;
                    _bitmap = value;
                }
                if (oldBitmap != null)
                    oldBitmap.Dispose();
            } 
        }
        public TileManager Owner { get; set; }
        public bool IsVisible { get; set; }
        public long itx;
        public long ity;
        public int zoomlevel;
        public int pixels;
        int taskcount;
        public bool busy;
        public int EstimatedWork;
        public bool HighPrio;

        public static long GlobalValueSum=0;

        public Tile(TileManager owner, long itx, long ity, int zoomlevel)
        {
            this.Owner = owner;
            this.IsVisible = true;
            this.itx = itx;
            this.ity = ity;
            this.zoomlevel = zoomlevel;
            this.taskcount = 0;
            busy = false;
            HighPrio = false;
            EstimatedWork = 0;

            double tilesize = TileManager.GetTilesize(zoomlevel);

            // Improve zoomin experience by using a tile for the previous zoomlevel as the starting point if possible
            long parent_itx = (long)Math.Floor(itx * 0.5);
            long parent_ity = (long)Math.Floor(ity * 0.5);
            Tile parent = owner.GetCachedTile(parent_itx, parent_ity, zoomlevel - 1);
            Bitmap b;
            if (parent != null && parent.pixels > 4)
            {
                b = GenerateBitmapFromParent(parent);
                // This tile represents a quarter of the parent. The estimated work to improve this tile
                // is one quarter of the work to improve the parent
                EstimatedWork = parent.EstimatedWork/4;
            }
            else
            {
                // Generate a synchronous bitmap quickly
                b = GenerateBitmap(itx * tilesize, ity * tilesize, tilesize, 2);
            }
            this.Bitmap = b;
            this.pixels = b.Width;

            // Start an improvement task if necessary
            InternalStartImproveBitmap();
        }

        private System.Drawing.Bitmap GenerateBitmapFromParent(Tile parent)
        {
            // Generate a half-resolution bitmap from the parent
            Bitmap pbm = parent.Bitmap;
            int ps = pbm.Width;
            int s = ps/2;
            Bitmap bm = new Bitmap(s, s);

            // Find the part of the source to use
            Rectangle r = new Rectangle();
            if ((itx & 1) == 0)
                r.X = 0;
            else
                r.X = s;

            if ((ity & 1) != 0)
                r.Y = 0;
            else
                r.Y = s;

            r.Width = r.Height = s;

            Graphics g = Graphics.FromImage(bm);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
            g.DrawImage(pbm, new Rectangle(0, 0, s, s), r, GraphicsUnit.Pixel);

            return bm;
        }

        public void StartImproveBitmap()
        {
            if (taskcount == 0)
                InternalStartImproveBitmap();
        }

        static int cpucount = Environment.ProcessorCount;
        static TaskFactory lowfactory  = new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(cpucount, ThreadPriority.Lowest));
        static TaskFactory highfactory = new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(cpucount, ThreadPriority.BelowNormal));

        // Long-running tasks are put in a low-priority queue and short-running tasks are put in a high-priority queue
        // This constant defines what amount of estimated work classifies a task as long- or short-running
        const int WorkLimit = 600*TilePixels*TilePixels;

        private void InternalStartImproveBitmap()
        {
            if (IsVisible && pixels < Tile.TilePixels)
            {
                Interlocked.Increment(ref taskcount);
                //Console.WriteLine("New    itx={0} ity={1} zoom={2} newsize={3} taskcount={4}", itx, ity, zoomlevel, pixels * 2, taskcount);
                if (EstimatedWork < WorkLimit)
                {
                    HighPrio = true;
                    highfactory.StartNew(() => ImproveBitmap());
                }
                else
                {
                    HighPrio = false;
                    lowfactory.StartNew(() => ImproveBitmap());
                }
            }
        }

        private void ImproveBitmap()
        {
            int newsize = pixels * 2;
            if (IsVisible && newsize <= Tile.TilePixels)
            {
                busy = true;
                Owner.NotifyTileChanged(this);
                Stopwatch sw = new Stopwatch();
                //Console.WriteLine("Start  itx={0} ity={1} zoom={2} newsize={3}", itx, ity, zoomlevel, newsize);
                sw.Start();
                double tilesize = TileManager.GetTilesize(zoomlevel);
                int work = this.EstimatedWork;
                Bitmap b = GenerateBitmap(itx * tilesize, ity * tilesize, tilesize, newsize);
                busy = false;
                if (b != null)
                {
                    this.Bitmap = b;
                    pixels = newsize;
                    Owner.NotifyTileChanged(this);
                }
                sw.Stop();
                //if (b == null)
                //    Console.WriteLine("ABORT  itx={0} ity={1} zoom={2} newsize={3} IsVisible={4} duration={5}ms taskcount={6}", itx, ity, zoomlevel, newsize, IsVisible, (int)sw.Elapsed.TotalMilliseconds, taskcount-1);
                //else
                //Console.WriteLine("Finish itx={0} ity={1} zoom={2} newsize={3} IsVisible={4} duration={5}ms EstimatedWork={6} {7}", itx, ity, zoomlevel, newsize, IsVisible, (int)sw.Elapsed.TotalMilliseconds, work, work<WorkLimit?"HIGH":"LOW");
                InternalStartImproveBitmap();
            }
            Interlocked.Decrement(ref taskcount);
        }

        public Bitmap GenerateBitmap(double x0, double y0, double tilesize, int pixels)
        {
            // x0,y0 indicates lower left bitmap corner
            y0 += tilesize; // Move origin to top right (bitmap origin)
            Bitmap b = new Bitmap(pixels, pixels);
            Rectangle r = new Rectangle(0, 0, pixels, pixels);
            BitmapData bd = b.LockBits(r, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            int bytes = bd.Height * bd.Stride;
            byte[] buf = new byte[bytes];
            double d = tilesize / pixels;
            int valuesum = 0;
            for (int iy = 0; iy < pixels; iy++)
            {
#if true
                // Generate 2 pixels at a time
                for (int ix = 0; ix < pixels; ix += 2)
                {
                    int value1, value2;
                    MandelValue2(x0 + d * 0.5 + ix * d, y0 - d * 0.5 - iy * d, x0 + d * 0.5 + (ix+1) * d, y0 - d * 0.5 - iy * d, out value1, out value2);
                    valuesum += value1+value2;
                    GlobalValueSum += value1 + value2;

                    int color = Owner.GetColor(value1);
                    int p = iy * bd.Stride + ix * 4;
                    buf[p + 0] = (byte)(color & 0xff);
                    buf[p + 1] = (byte)((color >> 8) & 0xff);
                    buf[p + 2] = (byte)((color >> 16) & 0xff);
                    buf[p + 3] = 0xff;

                    color = Owner.GetColor(value2);
                    p = p+4;
                    buf[p + 0] = (byte)(color & 0xff);
                    buf[p + 1] = (byte)((color >> 8) & 0xff);
                    buf[p + 2] = (byte)((color >> 16) & 0xff);
                    buf[p + 3] = 0xff;
                }
#else
                for (int ix = 0; ix < pixels; ix++)
                {
                    int value = MandelValue(x0 + d * 0.5 + ix * d, y0 - d * 0.5 - iy * d);
                    valuesum += value;
                    GlobalValueSum += value;
                    int color = TileManager.GetColor(value);
                    int p = iy * bd.Stride + ix * 4;
                    buf[p + 0] = (byte)(color & 0xff);
                    buf[p + 1] = (byte)((color >> 8) & 0xff);
                    buf[p + 2] = (byte)((color >> 16) & 0xff);
                    buf[p + 3] = 0xff;
                }
#endif
                if (!IsVisible)
                {
                    // Cancel bitmap generation
                    b.UnlockBits(bd);
                    return null;
                }
            }
            System.Runtime.InteropServices.Marshal.Copy(buf, 0, bd.Scan0, bytes);
            b.UnlockBits(bd);

            this.EstimatedWork = valuesum * 4;

            return b;
        }

        // Single pixel version
        // MOPS/s = 270
        private int MandelValue(double cx, double cy)
        {
            //Console.WriteLine("x={0} y={1}", x, y);
            // z = z^2 + c
            double x = cx;
            double y = cy;
            double zx, zy;
            int i = 0;
            int limit = Owner.limit;
            while (i < limit && x * x + y * y < 4)
            {
                zx = x * x - y * y + cx;
                zy = 2 * x * y + cy;
                x = zx;
                y = zy;
                i++;
            }

            return i;
        }

        // Try using Complex variables
        private int MandelValueC(double cx, double cy)
        {
            //Console.WriteLine("x={0} y={1}", x, y);
            // z = z^2 + c
            Complex c = new Complex(cx, cy);
            Complex z = new Complex();

            int i = 0;
            int limit = Owner.limit;
            while (i < limit && z.Real * z.Real + z.Imaginary * z.Imaginary < 4)
            {
                z = z * z + c;
                i++;
            }

            return i;
        }

        // Calculate 2 pixels at once
        // Attempts to keep more execution units busy in the CPU
        // MOPS/s = 420
        private void MandelValue2(double cx1, double cy1, double cx2, double cy2, out int result1, out int result2)
        {
            // z = z^2 + c

            double x1 = cx1;
            double y1 = cy1;
            double r1 = 0;
            double t1;

            double x2 = cx2;
            double y2 = cy2;
            double r2 = 0;
            double t2;

            int i1 = 0;
            int i2 = 0;

            int limit = Owner.limit;

            bool ready1 = false, ready2 = false;
            while (!(ready1 && ready2))
            {
                ready1 = i1 > limit || (r1 > 4);
                if (!ready1)
                {
                    r1 = x1 * x1 + y1 * y1;
                    t1 = x1 * x1 - y1 * y1 + cx1;
                    y1 = 2 * x1 * y1 + cy1;
                    x1 = t1;
                    i1++;
                }

                ready2 = i2 > limit || (r2 > 4);
                if (!ready2)
                {
                    r2 = x2 * x2 + y2 * y2;
                    t2 = x2 * x2 - y2 * y2 + cx2;
                    y2 = 2 * x2 * y2 + cy2;
                    x2 = t2;
                    i2++;
                }
            }

            result1 = i1 - 1;
            result2 = i2 - 1;
        }

        // Two pixels at a time with intermediate variables, seems to be slightly faster
        // MOPS/s = 420
        private void MandelValue3(double cx1, double cy1, double cx2, double cy2, out int result1, out int result2)
        {
            // z = z^2 + c

            double x1 = cx1;
            double y1 = cy1;
            double r1 = 0;

            double x2 = cx2;
            double y2 = cy2;
            double r2 = 0;

            int i1 = 0;
            int i2 = 0;

            int limit = Owner.limit;

            bool ready1 = false, ready2 = false;
            while (!(ready1 && ready2))
            {
                ready1 = i1 > limit || (r1 > 4);
                if (!ready1)
                {
                    double a1 = x1 * x1;
                    double b1 = y1 * y1;
                    double c1 = x1 * y1 * 2;
                    r1 = a1 + b1;
                    x1 = a1 - b1 + cx1;
                    y1 = c1 + cy1;
                    i1++;
                }

                ready2 = i2 > limit || (r2 > 4);
                if (!ready2)
                {
                    double a2 = x2 * x2;
                    double b2 = y2 * y2;
                    double c2 = x2 * y2 * 2;
                    r2 = a2 + b2;
                    x2 = a2 - b2 + cx2;
                    y2 = c2 + cy2;
                    i2++;
                }
            }

            result1 = i1 - 1;
            result2 = i2 - 1;
        }

        // Try using decimal to get higher precision
        // Turns out this is dramtically slower. MOPS/s < 1
        private void MandelValue4(double dcx1, double dcy1, double dcx2, double dcy2, out int result1, out int result2)
        {
            // z = z^2 + c

            decimal cx1 = (decimal)dcx1;
            decimal cy1 = (decimal)dcy1;
            decimal cx2 = (decimal)dcx2;
            decimal cy2 = (decimal)dcy2;

            decimal x1 = cx1;
            decimal y1 = cy1;
            decimal r1 = 0;

            decimal x2 = cx2;
            decimal y2 = cy2;
            decimal r2 = 0;

            int i1 = 0;
            int i2 = 0;

            int limit = Owner.limit;

            bool ready1 = false, ready2 = false;
            while (!(ready1 && ready2))
            {
                ready1 = i1 > limit || (r1 > 4);
                if (!ready1)
                {
                    decimal a1 = x1 * x1;
                    decimal b1 = y1 * y1;
                    decimal c1 = x1 * y1 * 2;
                    r1 = a1 + b1;
                    x1 = a1 - b1 + cx1;
                    y1 = c1 + cy1;
                    i1++;
                }

                ready2 = i2 > limit || (r2 > 4);
                if (!ready2)
                {
                    decimal a2 = x2 * x2;
                    decimal b2 = y2 * y2;
                    decimal c2 = x2 * y2 * 2;
                    r2 = a2 + b2;
                    x2 = a2 - b2 + cx2;
                    y2 = c2 + cy2;
                    i2++;
                }
            }

            result1 = i1 - 1;
            result2 = i2 - 1;
        }

        internal bool IntersectRect(double x00, double y00, double x01, double y01)
        {
            if (y00 > y01 || x00 > x01)
                throw new ArgumentException("Rectangle has wrong orientation");

            double tilesize = TileManager.GetTilesize(zoomlevel);
            double x10 = itx * tilesize;
            double y10 = ity * tilesize;
            double x11 = (itx + 1) * tilesize;
            double y11 = (ity + 1) * tilesize;

            // No intersection if one rectangle is outside the other in any direction
            if (x11 < x00 || x10 > x01 || y11 < y00 || y10 > y01)
                return false;
            else
                return true;
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.Bitmap = null;
        }

        #endregion
    }
}
