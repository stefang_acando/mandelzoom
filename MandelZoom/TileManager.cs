﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

/*
 * 
 * 
 */

namespace MandelZoom
{
    public class TileManager
    {
        static int[] _colortable;

        MandelForm Owner;
        public TileManager(MandelForm owner)
        {
            this.Owner = owner;
            InitColorTable();
            InitCache();
        }

        // This list stores all generated keys in order. It is used to delete the least-recently used tiles from the cache
        LinkedList<string> KeyList;
        Dictionary<string, Tile> TileCache;
        private void InitCache()
        {
            KeyList = new LinkedList<string>();
            TileCache = new Dictionary<string, Tile>();
        }

        public int limit = 10000;
        //const int COLORTABLE_SIZE = 32 * 32 * 32;
        const int COLORTABLE_SIZE = 256;
        private void InitColorTable()
        {
            if (_colortable == null)
            {
                ColorGenerator cg = new ColorGenerator();
                // Fire
                //cg.Add(new GradientPoint(0, Color.Red));
                //cg.Add(new GradientPoint(1, Color.Yellow));
                //cg.Add(new GradientPoint(2, Color.White));
                //cg.Add(new GradientPoint(3, Color.Yellow));
                //cg.Add(new GradientPoint(4, Color.Red));

                // Thermal
                //cg.Add(new GradientPoint(0, Color.DarkBlue));
                //cg.Add(new GradientPoint(1, Color.Magenta));
                //cg.Add(new GradientPoint(2, Color.Red));
                //cg.Add(new GradientPoint(3, Color.Orange));
                //cg.Add(new GradientPoint(4, Color.Yellow));
                //cg.Add(new GradientPoint(5, Color.White));

                // Hue
                //cg.Add(new GradientPoint(0, Color.Red));
                //cg.Add(new GradientPoint(1, Color.Magenta));
                //cg.Add(new GradientPoint(2, Color.Blue));
                //cg.Add(new GradientPoint(3, Color.Cyan));
                //cg.Add(new GradientPoint(4, Color.Green));
                //cg.Add(new GradientPoint(5, Color.Yellow));
                //cg.Add(new GradientPoint(6, Color.Red));

                // Rainbow
                //cg.Add(new GradientPoint(0, Color.Red));
                //cg.Add(new GradientPoint(1, Color.Orange));
                //cg.Add(new GradientPoint(2, Color.Yellow));
                //cg.Add(new GradientPoint(3, Color.Green));
                //cg.Add(new GradientPoint(4, Color.Blue));
                //cg.Add(new GradientPoint(5, Color.Indigo));
                //cg.Add(new GradientPoint(6, Color.Violet));

                // Grayscale
                //cg.Add(new GradientPoint(0, Color.Yellow));
                //cg.Add(new GradientPoint(1, Color.DarkGreen));
                //cg.Add(new GradientPoint(1, Color.Red));
                //cg.Add(new GradientPoint(2, Color.Black));

                // Hue with steps
                cg.Add(new GradientPoint(0, Color.Red));
                cg.Add(new GradientPoint(1, Color.Yellow));
                cg.Add(new GradientPoint(1, Color.Blue));
                cg.Add(new GradientPoint(2, Color.Cyan));
                cg.Add(new GradientPoint(2, Color.Green));
                cg.Add(new GradientPoint(3, Color.Yellow));
                cg.Add(new GradientPoint(3, Color.Red));
                cg.Add(new GradientPoint(4, Color.Black));
                _colortable = cg.Generate(COLORTABLE_SIZE);

                //_colortable = new int[COLORTABLE_SIZE];
                //int i = 0;
                //int r, g, b;
                //for (i = 0; i < COLORTABLE_SIZE; i++)
                //{
                //    double k = i * 360.0 / COLORTABLE_SIZE;
                //    g = hsv(k);
                //    r = hsv(k + 120);
                //    b = hsv(k + 240);
                //    _colortable[i] = (r << 16) + (g << 8) + b;
                //}

                //for (int r = 0; r < 32; r++)
                //{
                //    for (int g = 0; g < 32; g++)
                //    {
                //        for (int b = 0; b < 32; b++)
                //        {
                //            _colortable[i] = ((255 - r * 8) << 16) + ((g * 8) << 8) + (b * 8);
                //            i++;
                //        }
                //    }
                //}
            }
        }

        private static int hsv(double k)
        {
            if (k >= 360)
                k -= 360;

            double r;
            if (k < 60)
                r = k / 60;
            else if (k < 180)
                r = 1;
            else if (k < 240)
                r = 1 - (k - 180) / 60;
            else
                r = 0;

            // Mix in some white in the colors to make them slightly less brilliant
            const double w = 0.05;
            r = w + r * (1-w);

            return (int)(255 * r + 0.5);
        }

        public int GetColor(int value)
        {
            if (value == limit)
                return 0;
            // Note that this formula only works if COLORTABLE_SIZE is a power of 2
            value = value & (COLORTABLE_SIZE - 1);
            if (value == 0)
                value++;
            return _colortable[value];
        }

        // Gets a cached bitmap or create a low-quality bitmap and schedule it for improvement
        // Can also generate a bitmap from another zoomlevel as a starting point
        internal Tile GetTile(long itx, long ity, int k)
        {
            Tile t = GetCachedTile(itx, ity, k);
            if (t != null)
            {
                return t;
            }
            else
            {
                t = new Tile(this, itx, ity, k);
                StoreCachedTile(t, itx, ity, k);
                return t;
            }
        }

        private void StoreCachedTile(Tile t, long itx, long ity, int k)
        {
            lock (this)
            {
                string key = BuildKey(itx, ity, k);
                TileCache[key] = t;
                KeyList.AddLast(key);
                //Console.WriteLine("Added:" + key);

                // Delete the oldest tiles from the cache
                while (KeyList.Count > 1000)
                {
                    key = KeyList.First.Value;
                    t = null;
                    TileCache.TryGetValue(key, out t);
                    KeyList.RemoveFirst();
                    TileCache.Remove(key);
                    if (t != null)
                        t.Dispose();
                    //Console.WriteLine("Removed:"+key);
                }                
            }
        }

        private string BuildKey(long itx, long ity, int k)
        {
            return itx.ToString() + ":" + ity.ToString() + ":" + k.ToString();
        }

        public Tile GetCachedTile(long itx, long ity, int k)
        {
            lock (this)
            {
                string key = BuildKey(itx, ity, k);
                Tile t;
                bool ok = TileCache.TryGetValue(key, out t);
                return ok ? t : null;
                
            }
        }

        public static double GetTilesize(int k)
        {
            const double A = 1.0;
            const double B = 0.5;

            return A * Math.Pow(B, k);
        }

        internal void NotifyTileChanged(Tile tile)
        {
            long itx = tile.itx;
            long ity = tile.ity;
            int zoomlevel = tile.zoomlevel;
            Owner.NotifyTileChanged(itx, ity, zoomlevel);
        }

        public void SetVisibleTiles(double x0, double y0, double x1, double y1, int zoomlevel)
        {
            // x0,y0 = bottomleft
            // x1,y1 = topright
            //Console.WriteLine("SetVisibleTiles zoomlevel={0}", zoomlevel);

            // Mark all tiles not in the specified area as invisible
            foreach (Tile t in TileCache.Values)
            {
                if (t.zoomlevel != zoomlevel || !t.IntersectRect(x0,y0,x1,y1))
                {
                    // Make invisible
                    t.IsVisible = false;
                }
                else
                {
                    // Make visible
                    t.IsVisible = true;
                    t.StartImproveBitmap();
                }
            }
        }

        public void SetLimit(int newLimit)
        {
            if (newLimit != limit)
            {
                // A changed limit means that all tiles in the cache are unusable
                // tell all cached tiles that they are invisible so rendering stops
                // then empty the cache
                foreach (Tile t in TileCache.Values)
                {
                    // Make invisible
                    t.IsVisible = false;
                }

                InitCache();

                limit = newLimit;

            }
        }
    }
}
