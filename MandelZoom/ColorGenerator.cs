﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MandelZoom
{
    public class GradientPoint
    {
        public float pos;
        public Color color;
        public GradientPoint(float pos, Color color)
        {
            this.pos = pos;
            this.color = color;
        }
    }

    // Genererar en färgtabell genom att interpolera mellan ett antal färger med olika avstånd
    public class ColorGenerator
    {
        List<GradientPoint> plist = new List<GradientPoint>();
        public ColorGenerator()
        {
        }

        // GradientPoints should be added in order
        public void Add(GradientPoint p)
        {
            plist.Add(p);
        }

        public int[] Generate(int count)
        {
            if (plist.Count < 2)
                throw new InvalidOperationException("Must add at least two gradient points");

            int[] result = new int[count];
            float minpos = plist[0].pos;
            float maxpos = plist[plist.Count - 1].pos;
            int k = 0;
            for (int i = 0; i < count; i++)
            {
                float f = ((float)i) / (count-1);
                f = minpos + f * (maxpos - minpos);
                while (f > plist[k + 1].pos)
                    k++;
                // f ligger nu mellan k och k+1
                // mappa k till 0 och k+1 till 1
                float t = (f - plist[k].pos) / (plist[k + 1].pos - plist[k].pos);

                // Generera en ny färg genom att interpolera
                int r = (int)(plist[k].color.R + t * (plist[k + 1].color.R - plist[k].color.R) + 0.5f);
                int g = (int)(plist[k].color.G + t * (plist[k + 1].color.G - plist[k].color.G) + 0.5f);
                int b = (int)(plist[k].color.B + t * (plist[k + 1].color.B - plist[k].color.B) + 0.5f);

                int c = (r << 16) + (g << 8) + b;
                result[i] = c;
            }

            return result;
        }
    }
}
