﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace MandelZoom
{
    public partial class MandelForm : Form
    {
        bool ShowGrid = false;

        // Zoom state
        double x0,y0; // coordinates of topleft window corner
        int zoomlevel;        // zoomlevel 0=topmost zoom tilesize=A*B^zoomlevel

        // En tile identifieras av två tilenummer (ix, iy) och en zoomnivå (zoomlevel)
        // en tile täcker alltid 256x256 pixels
        // Numreringen utgår alltid från origo. 0,0 är den tile som har nedre vänstra hörnet i origo -1,0 ligger omedelbart till vänster

        // To calculate virtual coordinates from pixels:
        // k = tilesize/Tile.TilePixels
        // vx = x0+k*px
        // vy = y0-k*py

        TileManager tilemanager;

        public MandelForm()
        {
            this.DoubleBuffered = true;
            Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
            tilemanager = new TileManager(this);
            InitializeComponent();
            Reset();
        }

        private void Reset()
        {
            x0 = -2;
            y0 = 1;
            zoomlevel = 1;
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // Set options for fastest rendering
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel;
            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;

            // Find the tile containing the topleft pixel
            long itx, ity;
            GetTileForPixel(0, 0, out itx, out ity);
            Rectangle r = GetPixelRectForTile(itx, ity, zoomlevel);
            int sx = r.Left;
            int sy = r.Top;

            for (int ix = 0; sx+ix*Tile.TilePixels < this.Width; ix++)
            {
                for (int iy = 0; sy+iy*Tile.TilePixels < this.Height; iy++)
                {
                    r = new Rectangle(sx+ix*Tile.TilePixels, sy+iy*Tile.TilePixels, Tile.TilePixels, Tile.TilePixels);

                    if (r.IntersectsWith(e.ClipRectangle))
                    {
                        Tile t = tilemanager.GetTile(itx + ix, ity - iy, zoomlevel);
                        lock (t)
                        {
                            g.DrawImage(t.Bitmap, r);
                        }

                        if (ShowGrid)
                        {
                            // Display bitmap resolution
                            Color c = t.HighPrio ? Color.Red : Color.Black;
                            string s = string.Format("{0} {1:N3}", t.pixels, t.EstimatedWork/1000000.0);
                            Size sz = TextRenderer.MeasureText(s, this.Font);
                            TextRenderer.DrawText(g, s, this.Font, new Point(r.X + r.Width / 2 - sz.Width/2, r.Y + r.Height / 2 - sz.Height/2), Color.White, c);

                            // Show busy indication
                            if (t.busy)
                            {
                                Pen p;
                                if (t.HighPrio)
                                    p = Pens.Red;
                                else
                                    p = Pens.White;

                                const int C=4;
                                for (int i = 1; i < C; i++)
                                {
                                    g.DrawLine(p, r.X + r.Width * i / C, r.Y + r.Height, r.X + r.Width * i / C, r.Y);
                                    g.DrawLine(p, r.X, r.Y + r.Height * i / C, r.X + r.Width, r.Y + r.Height * i / C);
                                }
                            }
                        }
                    }
                }
            }
            if (ShowGrid)
            {
                // Draw grid
                for (int ix = 0; sx + ix * Tile.TilePixels < this.Width; ix++)
                {
                    g.DrawLine(Pens.White, sx + ix * Tile.TilePixels, 0, sx + ix * Tile.TilePixels, this.Height);
                }
                for (int iy = 0; sy + iy * Tile.TilePixels < this.Height; iy++)
                {
                    g.DrawLine(Pens.White, 0, sy + iy * Tile.TilePixels, this.Width, sy + iy * Tile.TilePixels);
                }
            }
        }

        Point LastMouseLocation;
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            // Handle panning
            if ((e.Button & System.Windows.Forms.MouseButtons.Left) != 0)
            {
                Size delta = (Size)e.Location - (Size)LastMouseLocation;
                // We now know how many pixels we want to move the window.
                // Convert this to virtual coordinates
                double tilesize = TileManager.GetTilesize(zoomlevel);
                x0 -= delta.Width * tilesize / Tile.TilePixels;
                y0 += delta.Height* tilesize / Tile.TilePixels;
                NotifyVisibilityChange();

                Invalidate();
            }
            LastMouseLocation = e.Location;

        }

        int LastWidth = 0;
        int LastHeight = 0;
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (LastWidth != 0 && LastHeight != 0)
            {
                double k = GetPixelSize();
                x0 += k * (LastWidth - Width) * 0.5;
                y0 -= k * (LastHeight - Height) * 0.5;
            }
            NotifyVisibilityChange();
            LastWidth = Width;
            LastHeight = Height;

            Invalidate();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // Do nothing - the whole screen is overwritten in paint
            //base.OnPaintBackground(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // Stop all threads
            zoomlevel = -1;
            NotifyVisibilityChange();
        }

        int LastMouseWheelDelta = 0;
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            //base.OnMouseWheel(e);

            // Handle scroll wheel (zoom)
            LastMouseWheelDelta += e.Delta;
            int d = LastMouseWheelDelta / 120;
            if (d != 0)
            {
                Zoom(d);
                LastMouseWheelDelta = 0;
            }
        }

        private void NotifyVisibilityChange()
        {
            // Calculate bounding rectangle (x0,y0) is topleft
            double k0 = TileManager.GetTilesize(zoomlevel) / Tile.TilePixels; ;
            double x1 = x0 + k0 * Width;
            double y1 = y0 - k0 * Height;

            tilemanager.SetVisibleTiles(x0, y1, x1, y0, zoomlevel);
        }

        internal void NotifyTileChanged(long itx, long ity, int z)
        {
            if (IsDisposed || zoomlevel<0)
                return;

            if (InvokeRequired)
            {
                try
                {
                    this.Invoke((Action)(() => NotifyTileChanged(itx, ity, z)));
                }
                catch (ObjectDisposedException)
                {
                    // Can happen during shutdown
                }
                return;
            }

            // Ignore if the invalid tile is not at the correct zoomlevel
            if (z != zoomlevel)
                return; 

            Rectangle R = GetPixelRectForTile(itx, ity, z);
            Invalidate(R);

        }

        // Returns the pixels covered by the specified tile
        // The tiles are guaranteed to be continous - no overlaps or gaps
        Rectangle GetPixelRectForTile(long itx, long ity, int z)
        {
            // Find the coordinate for the topleft tile on the screen first
            // Use multiplication in pixel space to find the wanted tile

            // Find topleft tile
            long itx0, ity0;
            GetTileForPixel(0, 0, out itx0, out ity0);

            // Find the pixel coordinate of the topleft pixel in the topleft tile
            double tilesize = TileManager.GetTilesize(zoomlevel);
            int sx = (int)Math.Floor(Tile.TilePixels * ((x0 - itx0 * tilesize) / tilesize));
            int sy = (int)Math.Floor(Tile.TilePixels * ((y0 - ity0 * tilesize) / tilesize));
            // sx and sy are now the coordinates of the topleft screen pixel within the topleft tile
            // Convert to screen coordinates of topleft pixel in tile
            sx = -sx;
            sy = sy - Tile.TilePixels;

            // Adjust coordinates in pixel space using integer multiplication
            sx += (int)(itx - itx0) * Tile.TilePixels;
            sy -= (int)(ity - ity0) * Tile.TilePixels;

            return new Rectangle(sx, sy, Tile.TilePixels, Tile.TilePixels);
        }

        // Returns the tile coordinates for the tile covered by the specified pixel
        void GetTileForPixel(int px, int py, out long itx, out long ity)
        {
            double vx, vy;
            GetVirtualForPixel(px, py, out vx, out vy);
            double tilesize = TileManager.GetTilesize(zoomlevel);
            itx = (long)Math.Floor(vx / tilesize);
            ity = (long)Math.Floor(vy / tilesize);

        }

        // Returns the virtual coordinates of the center of the specified pixel
        void GetVirtualForPixel(int px, int py, out double vx, out double vy)
        {
            // vx = x0+k*(px+0.5)
            // vy = y0-k*(py+0.5)
            double k = GetPixelSize();
            vx = x0 + k * (px + 0.5);
            vy = y0 - k * (py + 0.5);
        }

        // Returns the size of a pixel in virtual coordinates
        private double GetPixelSize()
        {
            return TileManager.GetTilesize(zoomlevel) / Tile.TilePixels;
        }

        long LastValueSum = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            long NewValueSum = Tile.GlobalValueSum;
            long delta = NewValueSum - LastValueSum;
            delta = delta * 1000 / timer1.Interval / 1000000;
            this.Text = "MOPS/s = " + delta.ToString();
            LastValueSum = NewValueSum;
        }

        private void MandelForm_DoubleClick(object sender, EventArgs e)
        {
            if (this.TopMost)
            {
                this.TopMost = false;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.TopMost = true;
                this.FormBorderStyle = FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void MandelForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && this.TopMost)
            {
                this.TopMost = false;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal;
            }

            if (e.KeyCode == Keys.G)
            {
                ShowGrid = !ShowGrid;
                Invalidate();
            }

        }

        private void Zoom(int d)
        {
            // k0 should use the original zoomlevel
            double k0 = TileManager.GetTilesize(zoomlevel) / Tile.TilePixels; ;

            // Delta directly affects zoomlevel
            zoomlevel += d;

            if (zoomlevel < 0)
                zoomlevel = 0;

            if (zoomlevel > 46)
                zoomlevel = 46;

            // x0,y0 needs to be adjusted so the point under the mouse cursor remains stationary

            // To calculate virtual coordinates from pixels:
            // k = tilesize/Tile.TilePixels
            // vx = x0+k*px
            // vy = y0-k*py

            // Calculate virtual coordinate for the point under the cursor
            // Change k
            // Calculate x1,y1 from px,py, k1, vx,vy
            // x1 = vx-k1*px
            // y1 = vy+k1*py

            int px = LastMouseLocation.X;
            int py = LastMouseLocation.Y;
            double k1 = TileManager.GetTilesize(zoomlevel) / Tile.TilePixels;
            double vx = x0 + k0 * px;
            double vy = y0 - k0 * py;
            x0 = vx - k1 * px;
            y0 = vy + k1 * py;

            NotifyVisibilityChange();

            Invalidate();

            this.Text = "Zoomlevel=" + zoomlevel.ToString();
        }

        private void MandelForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case '+':
                    Zoom(+1);
                    break;

                case '-':
                    Zoom(-1);
                    break;

                case 'p':
                    // Display center coordinates and image height
                    double k1 = TileManager.GetTilesize(zoomlevel) / Tile.TilePixels;

                    double cx = x0 + k1 * Width / 2;
                    double cy = y0 - k1 * Height / 2;
                    double h = k1 * Height;

                    Console.WriteLine("ArgCenterX = {0};", cx);
                    Console.WriteLine("ArgCenterY = {0};", cy);
                    Console.WriteLine("ArgTargetHeight = {0};", h);
                    Console.WriteLine();
                    break;

                case '1':
                    tilemanager.SetLimit(tilemanager.limit / 2);
                    this.Text = "Limit=" + tilemanager.limit.ToString();
                    Invalidate();
                    break;

                case '2':
                    tilemanager.SetLimit(tilemanager.limit * 2);
                    this.Text = "Limit=" + tilemanager.limit.ToString();
                    Invalidate();
                    break;

            }
        }
    }
}
