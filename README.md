# MandelZoom #

This is a highly interactive program to zoom into the Mandelbrot set.

The program is multithreaded to take full advantage of multi-core machines.

- Zoom in and out using the mousewheel or +/- keys
- Pan using the mouse